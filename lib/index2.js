'use strict';

var Hello = React.createClass({
    displayName: 'Hello',

    render: function render() {
        return React.createElement(
            'div',
            null,
            React.createElement(
                'h1',
                null,
                'Hello from Mustabshir'
            ),
            React.createElement(
                'p',
                null,
                'Maybe you prefer a hello from Adele'
            )
        );
    }
});

ReactDOM.render(React.createElement(Hello, null), document.getElementById('react-mk'));