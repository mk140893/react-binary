"use strict";

var Simpletable = React.createClass({
    displayName: "Simpletable",

    render: function render() {
        return React.createElement(
            "div",
            null,
            React.createElement(
                "table",
                { className: "table table-bordered" },
                React.createElement(
                    "thead",
                    null,
                    React.createElement(
                        "tr",
                        null,
                        React.createElement(
                            "th",
                            null,
                            "#"
                        ),
                        React.createElement(
                            "th",
                            null,
                            "Name"
                        ),
                        React.createElement(
                            "th",
                            null,
                            "Age"
                        ),
                        React.createElement(
                            "th",
                            null,
                            "Action"
                        )
                    )
                ),
                React.createElement("tbody", null)
            )
        );
    }
});

ReactDOM.render(React.createElement(Simpletable, null), document.getElementById('records'));