var Simpletable = React.createClass({ 
    render: function() { 
        return (
            <div>
               <table className="table table-bordered">
               <thead>
               <tr>
               		<th>#</th>
					<th>Name</th>
					<th>Age</th>
					<th>Action</th>
               </tr>
               </thead>
               <tbody></tbody>
               </table>
            </div>
            );
    }
}); 

ReactDOM.render(<Simpletable/>, document.getElementById('records'));