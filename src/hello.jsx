var Hello = React.createClass({ 
    render: function() { 
        return (
            <div>
                <h1>Hello from Kode</h1>
                <p>Maybe you prefer a hello from Adele</p>
            </div>
            );
    }
}); 

ReactDOM.render(<Hello/>, document.getElementById('react-target'));