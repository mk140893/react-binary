<?php

// Connect to database
	$connection=mysqli_connect('localhost','root','','binary64');

	$request_method=$_SERVER["REQUEST_METHOD"];
	switch($request_method)
	{
		case 'GET':
			// Retrive Products
			if(!empty($_GET["product_id"]))
			{
				$product_id=intval($_GET["product_id"]);
				get_details($product_id);
			}
			else
			{
				get_details();
			}
			break;
		case 'POST':
			// Insert Product
			insert_product();
			break;
		case 'PUT':
			// Update Product
			$product_id=intval($_GET["product_id"]);
			update_product($product_id);
			break;
		case 'DELETE':
			// Delete Product
			$info_id=intval($_GET["info_id"]);
			delete_product($info_id);
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;
	}
	function get_details()
	{
		global $connection;
		$query= "SELECT * FROM info";
		// if($product_id != 0)
		// {
		// 	$query.=" WHERE id=".$product_id." LIMIT 1";
		// }
		$response=array();
		$result=mysqli_query($connection, $query);
		while($row=mysqli_fetch_assoc($result))
		{
			$response[]=$row;
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

function insert_product()
	{
		global $connection;
		$name=$_POST["name"];
		$age=$_POST["age"];
		$query="INSERT INTO info SET name='{$name}', age='{$age}'";
		if(mysqli_query($connection, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Product Added Successfully.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Product Addition Failed.'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	function delete_product($info_id)
	{
		global $connection;
		$query="DELETE FROM info WHERE id=".$info_id;
		if(mysqli_query($connection, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Product Deleted Successfully.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Product Deletion Failed.'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

